from django import forms
from django.contrib.auth import views as auth_views
from django.contrib.auth import forms as auth_forms
from .models import User


class UserForm(auth_forms.UserCreationForm):
    class Meta:
        model = User
        fields = ['username',
        # 'password',
        'first_name', 'last_name', 'email']
        help_texts = {
            'username': None
        }
        widgets = {
            'username' : forms.TextInput(attrs = {'autofocus':'', 'placeholder': 'Enter your username'}),
            'email'    : forms.TextInput(attrs = {'placeholder': 'Enter your e-mail'}),
            'password'    : forms.TextInput(attrs = {'type':'password', 'placeholder': 'Enter your password'}),
            'first_name'    : forms.TextInput(attrs = {'placeholder': 'Enter your first name'}),
            'last_name'    : forms.TextInput(attrs = {'placeholder': 'Enter your last name'}),
        }
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields.get(field_name)
            # field.widget.attrs['placeholder'] = field.label
            field.label = ''

class UserLoginForm(auth_views.LoginView):

    template_name = 'login.html'
    # redirect_authenticated_user = True


from django.shortcuts import render, redirect
from .forms import UserForm, UserLoginForm
from .models import User
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login
from django.shortcuts import get_object_or_404

from .serializers import UserSerializer

from rest_framework import generics, permissions, mixins, serializers, viewsets, response
from oauth2_provider.contrib import rest_framework as oAuth2RestFramework
from oauth2_provider import models as oAuth2Models


# Create your views here.
class UserView(generics.RetrieveUpdateAPIView):
    authentication_classes = [oAuth2RestFramework.OAuth2Authentication]

    permission_classes = [
        oAuth2RestFramework.TokenHasReadWriteScope,
        oAuth2RestFramework.TokenHasScope
    ]

    required_scopes = ['read', 'write']

    serializer_class = UserSerializer

    lookup_field = "pk"

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

class UserViewSet(viewsets.ViewSet):
    authentication_classes = [oAuth2RestFramework.OAuth2Authentication]

    permission_classes = [
        oAuth2RestFramework.TokenHasReadWriteScope,
        oAuth2RestFramework.TokenHasScope
    ]

    required_scopes = ['read', 'write']

    def list(self, request):
        return response.Response('something')

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=request.user.pk)
        serializer = UserSerializer(user)
        return response.Response(serializer.data)

class VerifyCredentials(mixins.ListModelMixin, generics.GenericAPIView):
    authentication_classes = [oAuth2RestFramework.OAuth2Authentication]

    permission_classes = [
        oAuth2RestFramework.TokenHasReadWriteScope,
        oAuth2RestFramework.TokenHasScope
    ]

    required_scopes = ['read']

    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


def signup_view(request):
    context = {}

    if request.method == 'GET':

        context['form'] = UserForm(label_suffix='')

        return render(request, "auth.html", context)

    elif request.method == 'POST':
        formData = dict(request.POST.items())

        del formData['csrfmiddlewaretoken']

        if 'next' in formData:
            user = authenticate(request, **formData)
            login(request, user)

        if UserForm(formData).is_valid():
            formData['password'] = formData['password1']

            del formData['password1']

            del formData['password2']

            User.objects.create_user(**formData)

            return redirect('/login')

        context['form'] = UserForm(formData)

        return render(request, "auth.html", context)


from django.urls import path

from . import views
from .models import User
from .serializers import UserSerializer
from rest_framework import routers


urlpatterns = [
    path('verify_credentials', views.VerifyCredentials.as_view()),
    path('<int:pk>', views.UserView.as_view())
]

router = routers.DefaultRouter()
router.register(r'viewset', views.UserViewSet, basename='user')
urlpatterns = router.urls + urlpatterns

#!/bin/sh

# Stop execution at first error
set -e

python manage.py collectstatic --noinput

# Lauching server
uwsgi --socket :8000 --master --enable-threads --module server.wsgi

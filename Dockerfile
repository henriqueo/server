FROM python:3.8-alpine

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN apk add --no-cache mariadb-dev

RUN pip install -r /requirements.txt
RUN apk del .tmp

RUN mkdir /app
COPY . /app
WORKDIR /app
RUN source .env
COPY ./scripts /scripts
ENV PATH="${PATH}:/scripts"

RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

RUN adduser -D bibliotec
RUN chown -R bibliotec:bibliotec /vol
RUN chmod -R 755 /vol/web

USER bibliotec

# Launch application
CMD ["entrypoint.sh"]

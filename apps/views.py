from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework import viewsets, mixins, permissions, response

from apps.serializers import AppSerializerCreate, AppSerializerRetrieve

from . import models

# Create your views here.

class AppViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = models.App.objects.all()
    serializer_class = AppSerializerCreate
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        instance_serializer = AppSerializerRetrieve(instance)

        return response.Response(instance_serializer.data)


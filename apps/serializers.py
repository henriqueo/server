from rest_framework import serializers
from oauth2_provider import models as oauth2_models

class AppSerializerCreate(serializers.ModelSerializer):
    class Meta:
        model = oauth2_models.Application
        fields = ["client_type", "redirect_uris", "authorization_grant_type", "name"]

class AppSerializerRetrieve(serializers.ModelSerializer):
    class Meta:
        model = oauth2_models.Application
        fields = ["redirect_uris", "authorization_grant_type", "name", "client_id", "client_secret"]

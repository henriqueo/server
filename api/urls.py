from django.urls import path, include
from django.http import JsonResponse


urlpatterns = [
    path('users/', include('users.urls')),
    path('apps/', include('apps.urls')),
]

